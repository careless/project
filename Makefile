GROUP_FOLDERS =
IGNORED_FOLDERS =

RULES = init lint test deploy clean


.PHONY: all
all: lint test deploy


define RULE_TEMPLATE
.PHONY: $(1)
$(1):
ifneq ($$(unknown_folders),)
	$$(warning -----------------------------------------------------------------------)
	$$(warning > Encountered unknown folders: $$(unknown_folders))
	$$(warning > To resolve this error add them to GROUP_FOLDERS or IGNORED_FOLDERS)
	$$(warning -----------------------------------------------------------------------)
	$$(error Unknown folders: $$(unknown_folders))
endif

	@echo '>>> Run `make $(1)` for $$(GROUP_FOLDERS) <<<'
	@echo

	$(foreach _group, $(GROUP_FOLDERS),
	  @echo make -C $(_group) $(1)
	  @$(MAKE) -C $(_group) $(1)
	  @echo)
endef

all_folders := $(subst /,, $(dir $(wildcard */)))
folders := $(filter-out $(IGNORED_FOLDERS), $(all_folders))
unknown_folders := $(strip $(filter-out $(GROUP_FOLDERS), $(folders)))

# Generate all rules
$(foreach _rule, $(RULES), $(eval $(call RULE_TEMPLATE, $(_rule))))
