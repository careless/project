# Serverless Template
## Dependencies

- [Docker][docker]
- [GNU make][make] *preinstalled on pretty much every unix system*

## Multi Function Repository

- [Basics][basics]
- [Details][details]
  - [`handler`][details-handler]
  - [`app`][details-app]
  - [`serverless.yml`][details-serverless.yml]
  - [`make`][details-make]
  - [`docker`][details-docker]
- [Examples][examples]
  - [JavaScript][example-js]
  - [Python][example-python]
- [Links & Resources][links-resources]

## Basics

This repository is meant as a guideline and template for a serverless project for a collection of multi-language functions. The basic structure of the repository is as follows - for details see further below:

- `<function group>` - might be `user`, `order`, etc. think resource or logical group
  - `app/*` - the actual application logic of the functions; [details][details-app]
  - `handler.(py|js|...)` - entry point, handles provider specific stuff, delegates to `app/*`; [details][details-handler]
  - `serverless.yml` - serverless configuration; [details][details-serverless.yml]
  - `env/*` - contains environment specific configuration - e.g. a `dev` file, `prod` etc.; [details][details-env]
  - `Makefile` - provides easy to use "shortcuts" for common tasks: `make lint`, `make test`, `make deploy`; [details][details-make]
  - `Dockerfile` | `docker-compose.yml` | ... - ensures same behaviour regardless of local OS; [details][details-docker]

### `handler`

This file is meant to serve as a "translator" (anti-corruption layer) between the application logic and the provider (AWS, GC, Azure etc.). It receives events, extracts necessary data from the events and then delegates the call to specific parts of the application logic. It might also inject provider specific services to achieve inversion of control, examples include Azure KeyVault, AWS S3 Bucket, and others. The return value of application logic calls can then be used to build a response or write the data into some other part of the provider system.

Doing this allows us to keep the application logic separate from any provider specific logic, which means we can switch providers while only having to update the `handler` file.

### `app`

A folder which contains all application specific logic for the function group. Has no knowledge of any provider specific infastructure. Also contains tests.

See [examples][examples] to get an impression.

### `serverless.yml`

__The__ configuration file for the function group. Specifies the language, the event triggers, the function entrypoints and more.

See the [Serverless Framework Docs][serverless-docs] for further details, or take a look at one of the example files.

## `env`

This folder is meant to contain environment specific configuration files, such as `dev`, `prod` etc.. These files contain simple key-value pairs which - as the name suggests - specify environment variables which are then available in the docker container. Under the hood it uses the `env_file` `docker-compose` configuration to load the correct environment file (see [here for details][docker-env-file]).

The `env` files are not under version control - with exception of the `local` file - which makes them suitable for secret configuration values, such as provider authorization. In general, any provider specific configuration - which should be kept secret - belongs here.

### `make`

Each function group contains a `Makefile` which allows to perform the following tasks:
- `shell` - starts a shell in the docker container, eases development on a different OS
- `init` - installs dependencies
- `lint` - runs the language specific linter
- `test` - executes all tests
- `deploy` - deploys the function using `serverless`; expects an `ENV` specification - e.g. `ENV=dev make deploy`
- `clean` - cleans installed dependencies, docker containers and volumes

You can also just execute `make` which runs `lint`, `test`, and `deploy` in that order. Remember that `deploy` expects an `ENV` to be set. This `ENV` is also used as `stage` when deploying (`serverless deploy --stage "$ENV"`).

### `docker`

Each function is fully dockerized and meant to be developed from within the container. The files of interest in that regard are:
- `Dockerfile`
- `docker-compose.yml`
- `docker-entrypoint.sh`

This section is gonna take a very brief look at each one of these.

#### `Dockerfile`

Sets up the image to be used for the function. Based on [docker-lambda][docker-lambda] which emulates the AWS lambda environment as closely as possible which makes the template biased towards AWS but should ease development for it.

In addition to using the docker-lambda basis the Dockerfiles install additional software required for development, mainly the [Serverless Framework][serverless-docs]. This makes development and deployment seamlessly possible.

#### `docker-compose.yml`

Usually meant to be used to configure a number of containers depending on each other. Here we use it to specify a collection of configurations in a more directly accessible way.

One major configuration is the `volumes` section. It uses volumes which have been specified up top - speak named volumes - and then mounts them into the container. The way these volumes are allows us to reuse installed dependencies instead of having to install them anew for each new container.

Instantiated docker volumes are being removed when running `make clean`.

#### `docker-entrypoint.sh`

Not really a part of docker per se but rather a way to specify commands to the container in a "nicer" way. The script is configured as [entrypoint][docker-entrypoint] in the `Dockerfile` and receives the arguments of `docker run` and/or `docker exec` (which are being executed by `docker-compose`).

It then uses these arguments to execute a number of "default" operations which are basically mirrored in the `Makefile`.

## Examples

This repository ships with two example function group setups. One for JavaScript/NodeJS based functions and one for Python3.6 functions. The differences and setups of these function groups are discussed in the following sections.

### JavaScript / NodeJS

*[See the `js-function` folder for details](/serverless-template/nodejs)*

This function group is meant to use nodeJS 8.10. It uses `npm` for it's dependency management (not suprising) and has a number of helpful libraries preinstalled:
- [`aws-sdk`][js-aws-sdk] for obvious reasons
- [`eslint`][js-eslint] and a number of plugins for linting
- [`mocha`][js-mocha] and [`chai`][js-chai] for testing

The basic application structure is expected to look like this:
- `handler.js`
- `app/`
  - `my_resource.js`
  - `test/test_my_resource.js`

You'll find these example files with dummy implementations in the `js-function` folder. The tests are passing and the code fulfills the linting requirements. Use them as guidelines.

No additional pacakges and/or plugins for `serverless` are installed.

### Python

*[See the `python-function` folder for details](/serverless-template/python)*

This function group assumes Python3.6 as basis for writing the functions. As does the JavaScript function this function too comes with a number of libraries preinstalled:
- [`boto3`][python-aws-sdk] the Python AWS SDK
- [`pylint`][python-pylint] for linting

For testing the [`unittest`][python-unittest] library is being used.

The [docker-lambda][docker-lambda] image for Python comes preinstalled with [pipenv][python-pipenv]. As such it uses the new and shiny [Pipfile][python-pipfile] for dependency management. To install a new dependency simply run `pipenv install <my-dependency>` (or `pipenv install --dev <my-dev-dependency` for something like a testing lib) in the docker container. `pipenv` will automatically add the new dependency to your `Pipfile`.

The Python function is following the same structure as the JavaScript function:
- `handler.py`
- `app/`
  - `my_resource.py`
  - `tests/test_my_resource.py`

Again, you'll find example files with dummy implementations in the `python-function` folder. The tests are passing and the code fulfills the linting requirements. Use them as guidelines.

Just FYI, no action required from you: compared to JavaScript the dependency management for serverless functions in Python is a bit more difficult. As such the `python-function` ships with the [`serverless-python-requirements`][serverless-python-requirements] plugin preinstalled and configured. It takes care of packaging dependencies in a way which that AWS understands and makes them available in the lambda environment. This all happens automatically when running `make deploy`.

## Links & Resources
- [Docker][docker]
- [Make][make]
- [Serverless Framework](https://serverless.com/) ([Documentation][serverless-docs])
- [Serverless AWS Provider](https://www.gnu.org/software/make/manual/html_node/)


[basics]: #basics
[details]: #details
[details-app]: #app
[details-docker]: #docker
[details-handler]: #handler
[details-make]: #make
[details-env]: #env
[details-serverless.yml]: #serverlessyml
[examples]: #examples
[example-js]: #javascript
[example-python]: #python
[links-resources]: #links--resources

[docker]: https://docs.docker.com/
[make]: https://www.gnu.org/software/make/manual/html_node/
[docker-entrypoint]: https://docs.docker.com/engine/reference/builder/#entrypoint
[docker-lambda]: https://github.com/lambci/docker-lambda
[docker-env-file]: https://docs.docker.com/compose/compose-file/#env_file
[js-aws-sdk]: https://aws.amazon.com/sdk-for-node-js/
[js-eslint]: https://eslint.org/
[js-mocha]: https://mochajs.org/
[js-chai]: http://www.chaijs.com/
[python-aws-sdk]: https://aws.amazon.com/sdk-for-python/
[python-pylint]: https://www.pylint.org/
[python-pipenv]: https://docs.pipenv.org/
[python-pipfile]: https://github.com/pypa/pipfile
[python-unittest]: https://docs.python.org/3.6/library/unittest.html
[serverless-docs]: https://serverless.com/framework/docs/
[serverless-python-requirements]: https://github.com/UnitedIncome/serverless-python-requirements
